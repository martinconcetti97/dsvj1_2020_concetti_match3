#ifndef SCREENHANDLE_H
#define SCREENHANDLE_H

#include "raylib.h"

namespace match3 {
	namespace screen {
		
		enum TYPE_RES {
			_800X600,
			_1240X720,
			_1440X900
		};
		
		struct RESOLUTIONS {
			int WIDTH;
			int HEIGHT;
			TYPE_RES RES_SIZE;
		};
		
		extern RESOLUTIONS actualRes;
		
		extern bool onResizeWindow;

		extern int resolu;
		
		void initWindow();
		
		void deinitWindow();
		
		void resizeWindow(TYPE_RES whatRes);
		
	}
}
#endif // !SCREEN_H