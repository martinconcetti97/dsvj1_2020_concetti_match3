#include <iostream>

#include "screen.h"

namespace match3 {
	namespace screen {
		
		RESOLUTIONS actualRes;
		
		bool onResizeWindow;
		int resolu = 0;

		
		void initWindow() {
			
			actualRes.RES_SIZE = _1240X720;
			
			switch (actualRes.RES_SIZE)
			{
			case _800X600:
				actualRes.WIDTH = 800;
				actualRes.HEIGHT = 600;
				resolu = 0;
				break;
			case _1240X720:
				actualRes.WIDTH = 1240;
				actualRes.HEIGHT = 720;
				resolu = 1;
				break;
			case _1440X900:
				actualRes.WIDTH = 1440;
				actualRes.HEIGHT = 900;
				resolu = 2;
				break;
			}
			
			InitWindow(actualRes.WIDTH, actualRes.HEIGHT, "Match - v0.5");
			
		}
		
		void deinitWindow() {
			
			CloseWindow();
			
		}
		
		void resizeWindow(TYPE_RES whatRes) {
			
			switch (whatRes)
			{
			case match3::screen::_800X600:
				actualRes.WIDTH = 800;
				actualRes.HEIGHT = 600;
				SetWindowSize(actualRes.WIDTH, actualRes.HEIGHT);
				break;
			case match3::screen::_1240X720:
				actualRes.WIDTH = 1240;
				actualRes.HEIGHT = 720;
				SetWindowSize(actualRes.WIDTH, actualRes.HEIGHT);
				break;
			case match3::screen::_1440X900:
				actualRes.WIDTH = 1440;
				actualRes.HEIGHT = 900;
				SetWindowSize(actualRes.WIDTH, actualRes.HEIGHT);
				break;
			default:
				std::cout << "ERROR01: Warning! The resize of the window fail!" << std::endl;
				break;
			}
			
			onResizeWindow = false;
		}
		
	}
}