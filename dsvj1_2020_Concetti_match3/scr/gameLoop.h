#ifndef GAMELOOP_H
#define GAMELOOP_H

#include "raylib.h"



	namespace game {

		extern int screenWidth;
		extern int screenHeight;
		extern int screen;
		extern int letterSize;
		extern bool inGame;

		void execute();
	}

#endif
