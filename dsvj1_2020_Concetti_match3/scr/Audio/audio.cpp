#include "audio.h"


namespace game
{

	Sound match;
	Sound click;

	Music menu_song;
	Music gameplay_music;

	namespace audioftg {
		void loadAudio() {

			InitAudioDevice();
			match = LoadSound("res/assets/match.wav");
			click = LoadSound("res/assets/blip.wav");

			menu_song = LoadMusicStream("res/assets/Child's Nightmare.ogg");
			gameplay_music = LoadMusicStream("res/assets/Solve The Puzzle.ogg");

			PlayMusicStream(menu_song);

			SetMusicVolume(menu_song, static_cast<float>(0.40));
		}
		void unloadAudio() {

			UnloadSound(match);
			UnloadSound(click);

			UnloadMusicStream(menu_song);
			UnloadMusicStream(gameplay_music);

			CloseAudioDevice();

		}
	}
}