#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace game
{

	extern Sound match;
	extern Sound click;

	extern Music menu_song;
	extern Music gameplay_music;


	namespace audioftg {

		void loadAudio();
		void unloadAudio();
	}
}
#endif
