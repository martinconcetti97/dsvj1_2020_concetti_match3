#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"



	namespace game{
		extern Texture2D texture_piece1;
		extern Texture2D texture_piece2;
		extern Texture2D texture_piece3;
		extern Texture2D texture_piece4;

		extern Texture2D texture_bg1;
		extern Texture2D texture_bg2;



		//buttons
		extern Texture2D texture_button;
		extern Texture2D texture_button1;
		extern Texture2D texture_button2;
		extern Texture2D texture_button3;
		extern Texture2D texture_return;
		extern Texture2D texture_slider_right;
		extern Texture2D texture_slider_left;


		//screens - 1
		extern Texture2D texture_background;
		extern Texture2D texture_backcredits;
		extern Texture2D texture_instruccions;

		void LoadTextures();
	}

#endif
