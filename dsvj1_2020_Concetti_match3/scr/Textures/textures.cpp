#include "textures.h"

#include"Menu/Resolutions/resolution.h"



	namespace game {

		//piece
		static Image iPiece2;
		static Image iPiece3;
		static Image iPiece4;
		static Image iPiece1;
		//backgrounds
		static Image iBg1;
		static Image iBg2;

		//buttons image
		static Image iButton;
		static Image iButton1;
		static Image iButton2;
		static Image iReturn;
		static Image iButton3;

		static Image iSliderR;
		static Image iSliderL;

		//screen - 1 image
		static Image iBackground;
		static Image iBcredits;
		static Image iNstruccions;

		//piece
		Texture2D texture_piece1;
		Texture2D texture_piece2;
		Texture2D texture_piece3;
		Texture2D texture_piece4;
		//backgrounds
		Texture2D texture_bg1;
		Texture2D texture_bg2;


		//buttons textures
		Texture2D texture_button;
		Texture2D texture_button1;
		Texture2D texture_button2;
		Texture2D texture_button3;
		Texture2D texture_return;

		Texture2D texture_slider_right;
		Texture2D texture_slider_left;

		//screen - 1 textures
		Texture2D texture_background;
		Texture2D texture_backcredits;
		Texture2D texture_instruccions;
		void LoadTextures()
		{
			//piece
			iPiece1 = LoadImage("res/assets/piece_1.png");
			iPiece2 = LoadImage("res/assets/piece_2.png");
			iPiece3 = LoadImage("res/assets/piece_3.png");
			iPiece4 = LoadImage("res/assets/piece_4.png");
			//backgrounds
			iBg1 = LoadImage("res/assets/back3.png");
			iBg2 = LoadImage("res/assets/back2.png");

			// buttons load image
			iButton = LoadImage("res/assets/play.png");
			iButton1 = LoadImage("res/assets/credits.png");
			iButton2 = LoadImage("res/assets/i_button.png");
			iButton3 = LoadImage("res/assets/resolution.png");

			iSliderR = LoadImage("res/assets/blue_sliderRight.png");
			iSliderL = LoadImage("res/assets/blue_sliderLeft.png");
			iReturn = LoadImage("res/assets/return.png");


			// screens 1 load image
			iBackground = LoadImage("res/assets/bg.png");
			iBcredits = LoadImage("res/assets/imagen_credits.png");
			iNstruccions = LoadImage("res/assets/controls_screen.png");

			ImageResize(&iBackground, GetScreenWidth(), GetScreenHeight());
			ImageResize(&iBg1, GetScreenWidth(), GetScreenHeight());
			ImageResize(&iBg2, GetScreenWidth(), GetScreenHeight() / 2);

			if (resolution_s::resolution_v == 1) {
				ImageResize(&iPiece1, 35, 35);
				ImageResize(&iPiece2, 35, 35);
				ImageResize(&iPiece3, 35, 35);
				ImageResize(&iPiece4, 35, 35);

			}
			else if (resolution_s::resolution_v == 2) {
				ImageResize(&iPiece1, 40, 40);
				ImageResize(&iPiece2, 40, 40);
				ImageResize(&iPiece3, 40, 40);
				ImageResize(&iPiece4, 40, 40);

			}
			else if (resolution_s::resolution_v == 3) {
				ImageResize(&iPiece1, 50, 50);
				ImageResize(&iPiece2, 50, 50);
				ImageResize(&iPiece3, 50, 50);
				ImageResize(&iPiece4, 50, 50);
			}

			ImageResize(&iBcredits, GetScreenWidth(), GetScreenHeight());
			ImageResize(&iNstruccions, GetScreenWidth(), GetScreenHeight());

			//piece
			texture_piece2 = LoadTextureFromImage(iPiece2);
			texture_piece3 = LoadTextureFromImage(iPiece3);
			texture_piece4 = LoadTextureFromImage(iPiece4);
			texture_piece1 = LoadTextureFromImage(iPiece1);
			texture_slider_right = LoadTextureFromImage(iSliderR);
			texture_slider_left = LoadTextureFromImage(iSliderL);

			//backgrounds
			texture_bg1 = LoadTextureFromImage(iBg1);
			texture_bg2 = LoadTextureFromImage(iBg2);

			texture_button = LoadTextureFromImage(iButton);
			texture_button1 = LoadTextureFromImage(iButton1);
			texture_button2 = LoadTextureFromImage(iButton2);
			texture_button3 = LoadTextureFromImage(iButton3);

			texture_return = LoadTextureFromImage(iReturn);


			texture_background = LoadTextureFromImage(iBackground);
			texture_backcredits = LoadTextureFromImage(iBcredits);
			texture_instruccions = LoadTextureFromImage(iNstruccions);

			//piece
			UnloadImage(iPiece2);
			UnloadImage(iPiece3);
			UnloadImage(iPiece4);
			UnloadImage(iPiece1);

			//backgrounds
			UnloadImage(iBg1);
			UnloadImage(iBg2);


			UnloadImage(iBackground);
			UnloadImage(iBcredits);
			UnloadImage(iNstruccions);

			UnloadImage(iButton);
			UnloadImage(iButton1);
			UnloadImage(iButton2);
			UnloadImage(iSliderL);
			UnloadImage(iSliderR);
			UnloadImage(iReturn);


		}

	}
