#ifndef GAMEOVER_H
#define GAMEOVER_H

namespace game {
	extern int lose_game;
	extern bool result;
	namespace gameOver {

		void input();
		void draw();
	}
}
#endif 