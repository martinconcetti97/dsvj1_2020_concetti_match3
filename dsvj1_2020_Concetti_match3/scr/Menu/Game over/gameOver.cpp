#include "gameover.h"

#include"gameLoop.h"
#include"Gameplay/gameplayLoop.h"
#include "Gameplay/Grid/grid.h"

namespace game {
	int lose_game;
	bool result;
	namespace gameOver {
		void input() {
			if (gameover) {
				if (lose_game == 0) {

					lose_game++;
					if (points >= 1000) {
						result = true;
					}
					else {
						result = false;

					}

				}

				if (IsKeyPressed(KEY_ENTER)) {
					gameover = false;
					result = false;
					lose_game = 0;
					gameplay::init();
				}
			}
		}
		void draw() {
			if (gameover) {
				if (result) {
					DrawText("You  Win", GetScreenWidth() / 2 - 67, GetScreenHeight() / 7, letterSize - 10, SKYBLUE);

				}
				else {
					DrawText("You Lose", GetScreenWidth() / 2 - 67, GetScreenHeight() / 7, letterSize - 10, SKYBLUE);

				}

				DrawText("Game Over", GetScreenWidth() / 2 - 77, GetScreenHeight() / 10, letterSize - 10, RED);

				DrawText("Press ENTER to play again", GetScreenWidth() / 2 - 140, static_cast<int>(GetScreenHeight() / 4.8), letterSize - 20, BLUE);
			}
		}
	}
}