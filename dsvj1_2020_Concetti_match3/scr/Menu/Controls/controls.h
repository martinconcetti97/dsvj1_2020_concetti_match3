#ifndef CONTROLS_H
#define CONTROLS_H
#include "Menu/Menu/menu.h"

namespace game
{
	extern buttonplay return_from_i;

	namespace inst_s
	{
		void init();
		void input();
		void update();
		void draw();
	}
}
#endif 
