#include "controls.h"

#include "gameLoop.h"
#include"Menu/Credits/credits.h"
#include"Audio/audio.h"

namespace game
{
	buttonplay return_from_i;
	namespace inst_s
	{
		void init() {
			//LoadTextures();

			return_from_i.frameHeight = texture_button.height / NUM_FRAMES;
			return_from_i.sourceRec = { 0, 0, static_cast<float>(texture_button.width), static_cast<float>(return_from_i.frameHeight) };
			return_from_i.btnBounds = { static_cast<float>(GetScreenWidth() / 2 - texture_button.width / 2),
					static_cast<float>(GetScreenHeight() / 1 - 40 - texture_button.height / NUM_FRAMES / 2),  static_cast<float>(texture_button.width), static_cast<float>(return_from_i.frameHeight) };
			return_from_i.btnAction = false;
			return_from_i.btnState = 0;

			return_from_i.mousePoint = { 0.0f, 0.0f };
		}
		void input()
		{
			return_from_i.mousePoint = GetMousePosition();
			return_from_i.btnAction = false;

			if (CheckCollisionPointRec(return_from_i.mousePoint, return_from_i.btnBounds))
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
					return_from_i.btnState = 2;

				}
				else {
					return_from_i.btnState = 1;
				}

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {
					return_from_i.btnAction = true;
					PlaySound(click);
					screen = 0;

				}

			}
			else {
				return_from_i.btnState = 0;
			}

			return_from_i.sourceRec.y = static_cast<float>(return_from_i.btnState * return_from_i.frameHeight);
		}
		void update() {
			UpdateMusicStream(menu_song);

			inst_s::input();
		}
		void draw()
		{

			DrawTexture(texture_instruccions, 0, 0, WHITE);

			DrawTextureRec(texture_return, return_from_i.sourceRec, { return_from_i.btnBounds.x, return_from_i.btnBounds.y }, WHITE);
		}
	}
}