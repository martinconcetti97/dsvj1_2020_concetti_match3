#ifndef MENU_H
#define MENU_H

#include "Textures/textures.h"
#include "gameLoop.h"

namespace game {
	struct buttonplay {
		int frameHeight;
		Rectangle sourceRec;
		Rectangle btnBounds;
		bool btnAction;
		Vector2 mousePoint;
		int btnState;
	};
	const int NUM_FRAMES = 3;
	extern buttonplay bplay[4];
	namespace menu {

		void init();
		void input();
		void update();
		void draw();
	}
}
#endif
