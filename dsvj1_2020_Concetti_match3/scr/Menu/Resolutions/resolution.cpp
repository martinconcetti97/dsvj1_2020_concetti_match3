#include "resolution.h"

#include"Menu/Credits/credits.h"
#include"Audio/audio.h"
#include"Menu/Controls/controls.h"

	namespace game{
		namespace resolution_s{
			int resolution_v = 2;
			buttonplay2 bsettings[3];
			int numPosition1;
			int xPos;

			bool change_r = false;

			int randompos = 0;
			void init() {
				//	LoadTextures();
				change_r = false;
				for (int i = 0; i < 3; i++) {
					if (i == 0) {
						numPosition1 = 4;
						xPos = -GetScreenWidth() / 3;
						randompos = 0;
					}
					else if (i == 1) {
						numPosition1 = 4;
						xPos = GetScreenWidth() / 3;
						randompos = 0;
					}
					else if (i == 2) {
						numPosition1 = 1;
						xPos = 5;
						randompos = 100;
					}

					bsettings[i].frameHeight = texture_slider_right.height / NUM_FRAMES;
					bsettings[i].sourceRec = { 0, 0, static_cast<float>(texture_slider_right.width), static_cast<float>(bsettings[i].frameHeight) };
					bsettings[i].btnBounds = { static_cast<float>(GetScreenWidth() / 2 - texture_slider_right.width / 2 - xPos),
						static_cast<float>(GetScreenHeight() / numPosition1 - texture_slider_right.height / NUM_FRAMES / 2) - randompos,  static_cast<float>(texture_slider_right.width), static_cast<float>(bsettings[i].frameHeight) };

					bsettings[i].btnAction = false;
					bsettings[i].btnState = 0;

					bsettings[i].mousePoint = { 0.0f, 0.0f };
				}

			}
			void input()
			{
				if (resolution_v == 1) {
					screenWidth = 350;
					screenHeight = 550;
				}
				else if (resolution_v == 2) {

					screenWidth = 400;
					screenHeight = 600;
				}
				else if (resolution_v == 3) {

					screenWidth = 600;
					screenHeight = 800;
				}

				for (int i = 0; i < 3; i++) {

					bsettings[i].mousePoint = GetMousePosition();
					bsettings[i].btnAction = false;

					if (CheckCollisionPointRec(bsettings[i].mousePoint, bsettings[i].btnBounds))
					{
						if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
							bsettings[i].btnState = 2;

						}
						else {
							bsettings[i].btnState = 1;
						}

						if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {
							bsettings[i].btnAction = true;
							if (i == 0) {
								if (resolution_v < 3) {
									resolution_v++;

								}
							}
							else if (i == 1) {
								if (resolution_v > 1) {
									resolution_v--;

								}
							}
							else if (i == 2) {
								screen = 0;
								change_r = true;
								SetWindowSize(screenWidth, screenHeight);
								//LoadTextures();
								menu::init();
								credits_s::init();
								inst_s::init();
								init();

							}

							PlaySound(click);

						}

					}
					else {
						bsettings[i].btnState = 0;
					}

					bsettings[i].sourceRec.y = static_cast<float>(bsettings[i].btnState * bsettings[i].frameHeight);
				}

			}
			void update() {

				UpdateMusicStream(menu_song);

				resolution_s::input();
			}
			void draw()
			{
				DrawTexture(texture_background, GetScreenWidth() / 2 - texture_background.width / 2, GetScreenHeight() / 2 - texture_background.height / 2, WHITE);

				DrawText(FormatText("Resolution"), GetScreenWidth() / 2 - MeasureText("Resolution", biglet) / 2, smallet, biglet, SKYBLUE);


				for (int i = 0; i < 3; i++) {
					if (i == 0) {
						DrawTextureRec(texture_slider_right, bsettings[i].sourceRec, { bsettings[i].btnBounds.x, bsettings[i].btnBounds.y }, WHITE);
					}
					else if (i == 1) {
						DrawTextureRec(texture_slider_left, bsettings[i].sourceRec, { bsettings[i].btnBounds.x, bsettings[i].btnBounds.y }, WHITE);

					}
					else if (i == 2) {
						DrawTextureRec(texture_return, bsettings[i].sourceRec, { bsettings[i].btnBounds.x, bsettings[i].btnBounds.y }, WHITE);

					}
				}
				if (resolution_v == 1) {
					DrawText("350x550", static_cast<int>(GetScreenWidth() / 2.1) - MeasureText("Resolution", smallet) / 2, static_cast<int>(GetScreenWidth() / 3.2), smallet + 10, WHITE);

				}
				else if (resolution_v == 2) {
					DrawText("400x600", static_cast<int>(GetScreenWidth() / 2.1) - MeasureText("Resolution", smallet) / 2, static_cast<int>(GetScreenWidth() / 3.2), smallet + 10, WHITE);
				}
				else if (resolution_v == 3) {
					DrawText("600x800", static_cast<int>(GetScreenWidth() / 2.1) - MeasureText("Resolution", smallet) / 2, static_cast<int>(GetScreenWidth() / 3.2), smallet + 10, WHITE);

				}
			}
		}
	}

