#ifndef CREDITS_H
#define CREDITS_H

#include "Menu/Menu/menu.h"

namespace game
{

	static int biglet = 40;
	static int smallet = 20;
	extern buttonplay return_from_c;

	namespace credits_s
	{
		void init();
		void input();
		void update();
		void draw();
	}
}
#endif 
