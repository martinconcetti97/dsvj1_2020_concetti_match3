#include "grid.h"

#include <iostream>

#include "gameLoop.h"
#include "Textures/textures.h"
#include "Audio/audio.h"
#include"Menu/Resolutions/resolution.h"

namespace game {
	piece p[ROWS][COLUMNS];
	int m_button, p_counter, c_counter;
	int aux_color;
	int distance_x, distance_y;
	int distance = 6;
	int points, shift_limit;
	int dis_points;
	namespace ns_grid {
		static void resetV() {
			p_counter = 0;
			aux_color = 0;
			c_counter = 0;
			dis_points = 0;
		}
		void init() {
			srand((unsigned int)time(NULL));
			resetV();
			distance_x = 0;
			points = 0;
			distance_y = 0;
			shift_limit = 20;
			dis_points = 0;
			LoadTextures();
			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					if (resolution_s::resolution_v == 1) {
						p[i][j].size = { 35, 35 };

					}
					else if (resolution_s::resolution_v == 2) {

						p[i][j].size = { 40, 40 };

					}
					else if (resolution_s::resolution_v == 3) {

						p[i][j].size = { 55, 55 };
					}
					if (j == 0) {
						distance_x = 0;
						distance_y += 5;
					}
					p[i][j].speed = 300;
					if (resolution_s::resolution_v == 3) {
						p[i][j].position = { (GetScreenWidth() - p[i][j].size.x * 9 - p[i][j].size.x / 4) + p[i][j].size.x * j + distance_x, static_cast<int>(GetScreenHeight() / 2 + p[i][j].size.y / 1.3) + p[i][j].size.y * i + distance_y };

					}
					else {
						p[i][j].position = { (GetScreenWidth() - p[i][j].size.x * 9) + p[i][j].size.x * j + distance_x, static_cast<int>(GetScreenHeight() / 2 + p[i][j].size.y / 1.3) + p[i][j].size.y * i + distance_y };
					}

					distance_x += 5;
					p[i][j].random_piece = rand() % 4 + 1;
					// colors
					if (p[i][j].random_piece == 1) {
						p[i][j].color = BLUE;
					}
					else if (p[i][j].random_piece == 2) {
						p[i][j].color = BEIGE;
					}
					else if (p[i][j].random_piece == 3) {
						p[i][j].color = RED;
					}
					else if (p[i][j].random_piece == 4) {
						p[i][j].color = GREEN;
					}
					p[i][j].active = true;
					p[i][j].selected = false;
					p[i][j].reset = false;
				}
			}
		}
		static void resetPiece() {
			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					if (!p[i][j].active) {
						p[i][j].random_piece = rand() % 4 + 1;
						// colors
						if (p[i][j].random_piece == 1) {
							p[i][j].color = BLUE;
						}
						else if (p[i][j].random_piece == 2) {
							p[i][j].color = BEIGE;
						}
						else if (p[i][j].random_piece == 3) {
							p[i][j].color = RED;
						}
						else if (p[i][j].random_piece == 4) {
							p[i][j].color = GREEN;
						}
						p[i][j].active = true;
						p[i][j].selected = false;
						p[i][j].reset = true;

					}

				}
			}
		}
		void update() {
			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					if (p[i][j].active) {
						if (GetMousePosition().y >= p[i][j].position.y - p[i][j].size.y / 2
							&& GetMousePosition().y < p[i][j].position.y - p[i][j].size.y / 2 + p[i][j].size.y
							&& GetMousePosition().x >= p[i][j].position.x - p[i][j].size.x / 2
							&& GetMousePosition().x < p[i][j].position.x - p[i][j].size.x / 2 + p[i][j].size.x) {
							if (IsMouseButtonDown(m_button)) {

								if (p[i][j].selected == false) {
									if (aux_color == 0) {
										aux_color = p[i][j].random_piece;
										c_counter = 0;
									}
									if (aux_color == p[i][j].random_piece) {
										p[i][j].selected = true;
										p_counter++;
										c_counter++;
									}
									else if (aux_color != p[i][j].random_piece) {
										resetV();
									}
								}

							}
							else {
								for (int k = 0; k < ROWS; k++)
								{
									for (int l = 0; l < COLUMNS; l++)
									{
										if (p[k][l].selected == true && c_counter != 0) {
											if (p_counter >= 3) {
												if (p[i][j].random_piece == 1) {
													points = points + (p_counter * 1);

												}
												else if (p[i][j].random_piece == 2) {
													points = points + (p_counter * 2);

												}
												else if (p[i][j].random_piece == 3) {
													points = points + (p_counter * 3);

												}
												else if (p[i][j].random_piece == 4) {
													points = points + (p_counter * 4);

												}
												p[k][l].active = false;
												if (dis_points == 0) {
													shift_limit--;
													dis_points++;
													PlaySound(match);
												}
											}

										}
									}
								}
								resetV();
								for (int k = 0; k < ROWS; k++)
								{
									for (int l = 0; l < COLUMNS; l++)
									{
										p[k][l].selected = false;
									}
								}
							}

						}


					}
					if (GetMousePosition().y < p[0][j].position.y - p[0][j].size.y / 2
						|| GetMousePosition().y > p[5][j].position.y + p[5][j].size.y / 2
						|| GetMousePosition().x < p[i][0].position.x - p[i][0].size.x / 2
						|| GetMousePosition().x > p[i][7].position.x + p[i][7].size.x / 2) {
						resetV();
						for (int k = 0; k < ROWS; k++)
						{
							for (int l = 0; l < COLUMNS; l++)
							{

								resetV();
							}
						}
					}
				}
			}
			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					if (!p[i][j].active) {
						resetPiece();
					}
				}
			}
		}
		void draw() {
			//back-1
			DrawTexture(texture_bg1, 0, GetScreenHeight() / 2, WHITE);
			//back-2
			DrawTexture(texture_bg2, 0, 0, WHITE);

			//points
			if (shift_limit == 20) {
				DrawText("goal: reach 1000 points ", letterSize - 10, GetScreenHeight() / 4 + letterSize, letterSize - 10, ORANGE);

			}

			DrawText(FormatText("Points: %i/1000 ", points), 10, 10, 30, WHITE);
			DrawText(FormatText("Remaining Shifts: %i ", shift_limit), 10, GetScreenHeight() / 2 - 30, 30, WHITE);

			for (int i = 0; i < ROWS; i++)
			{
				for (int j = 0; j < COLUMNS; j++)
				{
					if (p[i][j].active)
					{
						if (p[i][j].random_piece == 1) {
							DrawTexture(texture_piece1, static_cast<int>(p[i][j].position.x - p[i][j].size.x / 2 + 3), static_cast<int>(p[i][j].position.y - p[i][j].size.y / 2 + 3), WHITE);

						}
						else if (p[i][j].random_piece == 2) {
							DrawTexture(texture_piece2, static_cast<int>(p[i][j].position.x - p[i][j].size.x / 2 + 3), static_cast<int>(p[i][j].position.y - p[i][j].size.y / 2 + 3), WHITE);

						}
						else if (p[i][j].random_piece == 3) {
							DrawTexture(texture_piece3, static_cast<int>(p[i][j].position.x - p[i][j].size.x / 2 + 3), static_cast<int>(p[i][j].position.y - p[i][j].size.y / 2 + 3), WHITE);

						}
						else if (p[i][j].random_piece == 4) {
							DrawTexture(texture_piece4, static_cast<int>(p[i][j].position.x - p[i][j].size.x / 2 + 3), static_cast<int>(p[i][j].position.y - p[i][j].size.y / 2 + 3), WHITE);

						}


					}
					if (p[i][j].selected == true) {
						DrawRectangleLines(static_cast<int>(p[i][j].position.x - p[i][j].size.x / 2 + 2), static_cast<int> (p[i][j].position.y - p[i][j].size.y / 2 + 2), static_cast<int>(p[i][j].size.x), static_cast<int>(p[i][j].size.y), WHITE);
					}
				}
			}
		}
	}
}