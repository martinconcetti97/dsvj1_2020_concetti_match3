#ifndef GRID_H
#define GRID_H

#include <cstdlib> // rand
#include <ctime> 

#include "raylib.h"

namespace game {

	const int ROWS = 6;
	const int COLUMNS = 8;

	struct piece {
		Vector2 position;
		Vector2 size;
		bool active;
		int random_piece;
		Color color;
		bool selected;
		float speed;
		bool reset;
	};
	extern piece p[ROWS][COLUMNS];
	extern int shift_limit;
	extern int points;
	namespace ns_grid {
		void init();
		void update();
		void draw();

	}

}
#endif
