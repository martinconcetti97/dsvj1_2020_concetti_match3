#include "gameplayLoop.h"

#include"gameLoop.h"
#include "Menu/Game over/gameover.h"
#include "Textures/textures.h"
#include "Audio/audio.h"
#include "Gameplay/Grid/grid.h"

using namespace game;
using namespace gameplay;

namespace game {
	bool gameover, pause;
	buttonplay return_from_g;

	float scrollingBack;
	float scrollingMid;
	float scrollingMid2;
	float scrollingFore;

	namespace gameplay {
		void init() {
			//button
			return_from_g.frameHeight = texture_button.height / NUM_FRAMES;
			return_from_g.sourceRec = { 0, 0, static_cast<float>(texture_button.width), static_cast<float>(return_from_g.frameHeight) };
			return_from_g.btnBounds = { static_cast<float>(GetScreenWidth() / 2 - texture_button.width / 2),
					static_cast<float>(GetScreenHeight() / 1 - 40 - texture_button.height / NUM_FRAMES / 2),  static_cast<float>(texture_button.width), static_cast<float>(return_from_g.frameHeight) };
			return_from_g.btnAction = false;
			return_from_g.btnState = 0;

			return_from_g.mousePoint = { 0.0f, 0.0f };

			//gameplay
			gameover = false;
			pause = false;

			ns_grid::init();

			LoadTextures();

			scrollingBack = 0.0f;
			scrollingMid = 0.0f;
			scrollingMid2 = 0.0f;
			scrollingFore = 0.0f;
		}
		void input() {
			if (pause) {
				return_from_g.mousePoint = GetMousePosition();
				return_from_g.btnAction = false;
				if (CheckCollisionPointRec(return_from_g.mousePoint, return_from_g.btnBounds))
				{
					if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {

						return_from_g.btnState = 2;
						PlaySound(click);
						screen = 0;

					}
					else {
						return_from_g.btnState = 1;

					}

				}
				else {
					return_from_g.btnState = 0;
				}
				return_from_g.sourceRec.y = static_cast<float>(return_from_g.btnState * return_from_g.frameHeight);

			}
		}
		void update() {
			if (IsKeyPressed('M')) {
				StopMusicStream(gameplay_music);
				muteMusicbool = !muteMusicbool;
			}
			if (IsKeyPressed('P')) {
				pause = !pause;
				StopMusicStream(gameplay_music);

			}
			if (!pause) {
				//----------------------------------------------------------------------------------     
				if (!muteMusicbool)
				{
					UpdateMusicStream(gameplay_music);
					SetMusicVolume(gameplay_music, static_cast<float>(0.40));
				}
				//restart
				if (shift_limit == 0) {
					gameover = true;
				}
				if (points >= 1000) {
					gameover = true;
				}
				if (gameover) {
					gameOver::input();
				}
				//player update
				if (!gameover) {
					ns_grid::update();
				}
			}
			gameplay::input();

		}
		void draw() {
			ns_grid::draw();
			//score in screen

			if (pause) {
				DrawText("GAME PAUSED", GetScreenWidth() / 2 - (MeasureText("GAME PAUSED", letterSize) / 2), GetScreenHeight() / 2 - letterSize, letterSize, BLACK);
				DrawTextureRec(texture_return, return_from_g.sourceRec, { return_from_g.btnBounds.x, return_from_g.btnBounds.y }, WHITE);
			}
			gameOver::draw();

		}

	}
}