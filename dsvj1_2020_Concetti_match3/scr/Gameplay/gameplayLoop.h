#ifndef GAMEPLAYLOOP_H
#define GAMEPLAYLOOP_H

#include "raylib.h"
#include "Menu/Menu/menu.h"
namespace game {
	extern bool gameover;
	extern bool pause;
	static bool muteMusicbool;

	extern buttonplay return_from_g;

	namespace gameplay {
		void init();
		void input();
		void update();
		void draw();
	}
}
#endif 