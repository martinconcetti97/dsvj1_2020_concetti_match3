#include"gameLoop.h"

#include <iostream>

#include "Menu/Menu/menu.h"
#include"Gameplay/gameplayLoop.h"
#include "Menu/Credits/credits.h"
#include "Menu/Controls/controls.h"
#include "Audio/audio.h"
#include "Menu/Resolutions/resolution.h"


	namespace game {
		int screenWidth;
		int screenHeight;
		int letterSize;
		int screen;
		int s_once_credits = 0, s_once_inst = 0, s_once_r = 0;
		bool inGame;

		static void init() {
			inGame = true;

			screenWidth = 400;
			screenHeight = 600;
			screen = 0;
			letterSize = 40;
			InitWindow(screenWidth, screenHeight, "MATCH3 v1");
			menu::init();
			audioftg::loadAudio();
		}
		static void update() {
			switch (screen) {
			case 0:
				PlayMusicStream(menu_song);
				menu::update();
				break;
			case 1:
				StopMusicStream(menu_song);
				if (!IsSoundPlaying(click)) {
					PlayMusicStream(gameplay_music);

				}
				gameplay::update();
				break;
			case 2:
				if (s_once_credits == 0)
				{
					credits_s::init();
					s_once_credits++;
				}
				credits_s::update();
				break;
			case 3:
				if (s_once_inst == 0)
				{
					inst_s::init();
					s_once_inst++;
				}
				inst_s::update();
				break;
			case 4:
				if (s_once_r == 0)
				{
					resolution_s::init();
					s_once_r++;
				}
				resolution_s::update();
				break;
			}

		}
		static void draw() {
			BeginDrawing();
			ClearBackground(WHITE);

			switch (screen) {
			case 0:
				menu::draw();
				break;
			case 1:
				gameplay::draw();
				break;
			case 2:
				credits_s::draw();
				break;
			case 3:
				inst_s::draw();
				break;
			case 4:
				resolution_s::draw();
				break;
			}


			EndDrawing();
		}

		static void deInit() {
			audioftg::unloadAudio();

		}

		void execute() {
			init();
			while (inGame && !WindowShouldClose()) {
				update();
				draw();
			}
			deInit();
			CloseWindow();
		}

	}

